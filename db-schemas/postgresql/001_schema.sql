CREATE SCHEMA account_info;
CREATE SCHEMA link_info;

CREATE TABLE account_info.account(
  id BIGSERIAL PRIMARY KEY,
  registration TIMESTAMP NOT NULL DEFAULT now(),
  latest_connection TIMESTAMP,
  name VARCHAR(120) NOT NULL UNIQUE,
  email VARCHAR(120) UNIQUE,
  entity_type VARCHAR(100) NOT NULL,
  password VARCHAR(120) NOT NULL,
  enabled BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE account_info.authority(
  id SMALLSERIAL PRIMARY KEY,
  description VARCHAR(180) NOT NULL,
  name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE account_info.account_authorities(
  account_id BIGINT NOT NULL REFERENCES account_info.account(id),
  authority_id SMALLINT NOT NULL REFERENCES account_info.authority(id),
  PRIMARY KEY(account_id, authority_id)
);

CREATE TABLE account_info.refresh_token(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  account_id BIGINT NOT NULL REFERENCES account_info.account(id),
  expires_at TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL
);

CREATE TABLE account_info.confirmation_token(
  id BIGINT PRIMARY KEY REFERENCES account_info.account(id),
  token TEXT NOT NULL UNIQUE,
  confirmed_at TIMESTAMP,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  expires_at TIMESTAMP NOT NULL
);

CREATE TABLE link_info.link(
  id BIGSERIAL PRIMARY KEY,
  owner_id BIGINT NOT NULL REFERENCES account_info.account(id),
  url TEXT NOT NULL,
  clicks_count BIGINT NOT NULL DEFAULT 0,
  created TIMESTAMP NOT NULL DEFAULT now()
);
