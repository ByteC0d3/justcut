CREATE PROCEDURE account_info.add_authority(
  name VARCHAR(50), description VARCHAR(180)) AS $$
BEGIN
    INSERT INTO account_info.authority(name, description)
        VALUES(name, description);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION account_info.generate_user_name()
RETURNS TRIGGER AS $$
DECLARE
    account_val bigint;
BEGIN
    IF (NEW.name IS NULL) OR (NEW.name = '') THEN
        SELECT currval('account_info."account_id_seq"') INTO account_val;
        NEW.name := concat('user-', account_val);
    END IF;
    RETURN NEW;
END; 
$$ LANGUAGE plpgsql;

CREATE TRIGGER generate_user_name_trigger
  BEFORE INSERT ON account_info.account
    FOR EACH ROW EXECUTE FUNCTION account_info.generate_user_name();
