CALL account_info.add_authority('MANAGE_ACCOUNTS', 'Create/update/delete accounts');
CALL account_info.add_authority('MANAGE_OWN_LINKS', 'Manage own links in account');
CALL account_info.add_authority('MANAGE_ALL_LINKS', 'View and delete links');
CALL account_info.add_authority('CREATE_LINKS', 'Creating new links');
