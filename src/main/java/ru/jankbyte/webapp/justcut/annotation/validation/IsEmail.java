package ru.jankbyte.webapp.justcut.annotation.validation;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.FIELD;

/**
 * The annotation for validating email.
 * <p>String that's validating should'nt be empty,
 * and should matching like email.</p>
 */
@Retention(RUNTIME)
@Target(FIELD)
@Email(message = "Invalid email")
@NotEmpty(message = "Email is empty")
@Constraint(validatedBy = {})
public @interface IsEmail {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
