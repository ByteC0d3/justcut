package ru.jankbyte.webapp.justcut.annotation.validation;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import static ru.jankbyte.webapp.justcut.util.RegExUtils.URL_REGEX;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.FIELD;

/**
 * The annotation for validating string to URL.
 * <p>Examples of valid values:</p>
 * <ul>
 *   <li>https://google.com</li>
 *   <li>https://google.com/something</li>
 *   <li>http://mydomain.xxx:9090</li>
 * </ul>
 */
@Retention(RUNTIME)
@Target(FIELD)
@Pattern(regexp = URL_REGEX, message = "Invalid URL")
@Constraint(validatedBy = {})
public @interface IsURL {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
