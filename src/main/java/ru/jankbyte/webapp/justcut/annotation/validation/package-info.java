/**
 * Contains annotations for validating entity of requests.
 */
package ru.jankbyte.webapp.justcut.annotation.validation;
