package ru.jankbyte.webapp.justcut.component;

import org.springframework.stereotype.Component;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;

import ru.jankbyte.webapp.justcut.mapper.AccountMapper;
import ru.jankbyte.webapp.justcut.exception.notfound.AccountNotFoundException;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.model.account.Authority;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import ru.jankbyte.webapp.justcut.config.property.DefaultAccounts;
import ru.jankbyte.webapp.justcut.dto.registration.AccountDto;
import ru.jankbyte.webapp.justcut.service.AccountService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Adding base-managment accounts into database.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultAccountCreator {
    private final DefaultAccounts accountsConfig;
    private final PasswordEncoder encoder;
    private final AccountService accountService;
    private final AccountMapper mapper;

    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void createAccounts() {
        List<AccountDto> accounts = accountsConfig.accounts();
        if ((!accountsConfig.enabled()) || (accounts == null)
                || (accounts != null && accounts.isEmpty())) {
            return;
        }
        log.info("Creating managment accounts after startup");
        accounts.forEach(accountDto -> {
            Account rawAccount =
                mapper.convertAccountDtoToAccount(accountDto);
            rawAccount.setEnabled(true);
            try {
                accountService.getByName(rawAccount.getName());
                // TODO: validate password, authorites
            } catch (AccountNotFoundException exp) {
                accountService.createAccount(rawAccount);
            }
        });
    }
}
