package ru.jankbyte.webapp.justcut.component;

import org.springframework.stereotype.Component;

@Component
public class LongConversion {
    private final String allowedString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private final char[] allowedCharacters = allowedString.toCharArray();
    private final int base = allowedCharacters.length;

    public String encode(long input) {
        StringBuilder encodedString = new StringBuilder();
        if(input == 0) {
            return String.valueOf(allowedCharacters[0]);
        }
        while (input > 0) {
            encodedString.append(
                allowedCharacters[(int) (input % base)]);
            input = input / base;
        }
        return encodedString.reverse().toString();
    }

    public long decode(String input) {
        char[] characters = input.toCharArray();
        int length = characters.length;
        long decoded = 0L;
        int counter = 1;
        for (int i = 0; i < length; i++) {
            decoded += allowedString.indexOf(
                characters[i]) * Math.pow(base, length - counter);
            counter++;
        }
        return decoded;
    }
}