package ru.jankbyte.webapp.justcut.component.scheduled;

import ru.jankbyte.webapp.justcut.service.ConfirmationTokenService;

import org.springframework.stereotype.Component;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Component-scheduler that's clean expired confirmation
 * tokens and accounts.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExpiredAccountsCleaner {
    private final ConfirmationTokenService confirmationTokenService;

    /**
     * Method that's scheduling by spring-core for cleaning tokens.
     */
    @Scheduled(fixedDelay = 24, timeUnit = TimeUnit.HOURS)
    public void removeExpiredConfirmationTokensAndAccounts() {
        log.info("Cleaning expired confirmation tokens with accounts");
        confirmationTokenService.removeExpiredConfirmationTokens();
    }
}
