package ru.jankbyte.webapp.justcut.component.scheduled;

import org.springframework.stereotype.Component;
import org.springframework.scheduling.annotation.Scheduled;
import ru.jankbyte.webapp.justcut.service.authentication.refreshtoken.RefreshTokenService;

import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

/**
 * Component-scheduler that's clean expired refresh-tokens.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RefreshTokenCleaner {
    private final RefreshTokenService refreshTokenService;

    /**
     * Method that's run for cleaning refresh-tokens.
     */
    @Scheduled(fixedDelay = 24, timeUnit = TimeUnit.HOURS)
    public void removeExpiredRefreshTokens() {
        log.info("Cleaning expired refresh tokens");
        refreshTokenService.removeExpiredTokens();
    }
}
