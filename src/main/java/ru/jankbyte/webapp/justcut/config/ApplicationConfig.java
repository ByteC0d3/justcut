package ru.jankbyte.webapp.justcut.config;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * The main config for configure spring-boot properties
 * by annotations.
 */
@EnableScheduling
@ConfigurationPropertiesScan
@Configuration(proxyBeanMethods = false)
public class ApplicationConfig {
}
