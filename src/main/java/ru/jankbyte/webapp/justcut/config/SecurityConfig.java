package ru.jankbyte.webapp.justcut.config;

import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import ru.jankbyte.webapp.justcut.security.filter.JwtAuthenticationFilter;
import ru.jankbyte.webapp.justcut.security.filter.SecurityExceptionsProcessor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.core.annotation.Order;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The security config.
 */
@Slf4j
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {
    private final String[] PERMIT_ALL = {
        "/api/authentication/login", "/api/authentication/refresh",
        "/api/signup", "/api/signup/confirm*",
        "/redirect/**", "/swagger-ui/**", "/swagger",
        "/api-docs/**", "/web-res/**", "/error"
    };

    /**
     * The filter chain of http-requests.
     * @param http The chain-builder
     * @param jwtFilter Filter based on JWT-verifications
     * @param processor The exceptions-processor that's
     * implements {@link AccessDeniedHandler} and {@link AuthenticationEntryPoint}
     * @return The security filter chain
     */
    @Bean
    public SecurityFilterChain apiSecuredFilterChain(
            HttpSecurity http, JwtAuthenticationFilter jwtFilter,
            SecurityExceptionsProcessor processor) throws Exception {
        return http.csrf(csrf -> csrf.disable())
            .sessionManagement(session ->
                session.sessionCreationPolicy(STATELESS))
            .authorizeHttpRequests(request ->
                request.requestMatchers(PERMIT_ALL).permitAll()
                    .anyRequest().authenticated())
            .addFilterBefore(jwtFilter,
                UsernamePasswordAuthenticationFilter.class)
            // TODO: getting processor from here, and put into
            // JwtAuthenticationFilter
            .exceptionHandling(handler ->
                handler.authenticationEntryPoint(processor)
                    .accessDeniedHandler(processor))
            .build();
    }

    /**
     * Component for checking {@link UserDetails}.
     * <p>For example: that's check enabling/expiring of details.</p>
     * @return User details checker
     */
    @Bean
    public UserDetailsChecker detailsChecker() {
        return new AccountStatusUserDetailsChecker();
    }

    /**
     * Manager for authenticate and validate user credentials.
     * @param config Getting {@link AuthenticationManager} from this component by
     * config.getAuthenticationManager()
     * @return The authentication manager
     */
    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    /**
     * The password encoder for users password.
     * @return Password encoder with BCrypt implementation
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
