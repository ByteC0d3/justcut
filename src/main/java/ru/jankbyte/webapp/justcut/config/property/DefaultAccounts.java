package ru.jankbyte.webapp.justcut.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import ru.jankbyte.webapp.justcut.dto.registration.AccountDto;
import java.util.List;

@ConfigurationProperties(prefix = "app.default-accounts")
public record DefaultAccounts(boolean enabled, List<AccountDto> accounts) {
}
