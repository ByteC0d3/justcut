package ru.jankbyte.webapp.justcut.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app.email-confirmation")
public record EmailConfirmationProperties(String email,
        Boolean enabled, ExpiredTimeProperties timeExpiring) {
}
