package ru.jankbyte.webapp.justcut.config.property;

import lombok.Setter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;
import java.time.Duration;

/**
 * Base class that's provide fields for storing
 * time-control (expiring) information.
 */
@Getter
@RequiredArgsConstructor
public class ExpiredTimeProperties {
    private final TimeUnit timeUnit;
    private final long expiresAfter;

    /**
     * Getting milliseconds from combination time unit with expires after.
     * @return Time in milliseconds
     */
    public long getTimeInMilliseconds() {
        Duration duration = switch (timeUnit) {
            case SECONDS -> Duration.ofSeconds(expiresAfter);
            case MINUTES -> Duration.ofMinutes(expiresAfter);
            case HOURS -> Duration.ofHours(expiresAfter);
            case DAYS -> Duration.ofDays(expiresAfter);
            default -> throw new IllegalArgumentException(
                "Unsupported argument for time: %s".formatted(timeUnit));
        };
        return duration.toMillis();
    }
}
