package ru.jankbyte.webapp.justcut.config.property;

import java.util.concurrent.TimeUnit;
import lombok.Getter;

import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@ConfigurationProperties(prefix = "app.jwt")
public class JwtTokenProperties extends ExpiredTimeProperties {
    private final String secret;

    public JwtTokenProperties(String secret,
            TimeUnit timeUnit, long expiresAfter) {
        super(timeUnit, expiresAfter);
        this.secret = secret;
    }
}
