package ru.jankbyte.webapp.justcut.config.property;

import java.util.concurrent.TimeUnit;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app.refresh-token")
public class RefreshTokenProperties extends ExpiredTimeProperties {
    public RefreshTokenProperties(TimeUnit timeUnit, long expiresAfter) {
        super(timeUnit, expiresAfter);
    }
}
