package ru.jankbyte.webapp.justcut.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import ru.jankbyte.webapp.justcut.dto.login.*;
import ru.jankbyte.webapp.justcut.service.authentication.JwtAuthenticationService;

import lombok.RequiredArgsConstructor;
import jakarta.validation.Valid;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

@Tag(name = "JWT authentication controller",
    description = "Provides API for getting/updating JWT")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = {"/api/authentication"},
    consumes = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {
    private final JwtAuthenticationService jwtAuthService;

    @Operation(summary = "Getting JWT",
        description = "Getting JWT by account credentials")
    @PostMapping("/login")
    public JwtAuthenticationResponse getJWT(
            @RequestBody @Valid AuthenticationRequest request) {
        return jwtAuthService.createTokens(request);
    }

    @Operation(summary = "Refreshing tokens",
        description = "Refreshing JWT by refresh token")
    @PostMapping("/refresh")
    public JwtAuthenticationResponse refreshJWT(
            @RequestBody @Valid RefreshTokenRequest refreshTokenRequest) {
        return jwtAuthService.refreshTokens(refreshTokenRequest);
    }
}
