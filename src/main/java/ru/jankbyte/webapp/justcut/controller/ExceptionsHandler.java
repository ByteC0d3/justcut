package ru.jankbyte.webapp.justcut.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ProblemDetail;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.ObjectError;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.MethodArgumentNotValidException;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

@Slf4j
@RestControllerAdvice(basePackages = "ru.jankbyte.webapp.justcut.controller")
public class ExceptionsHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatusCode status, WebRequest request) {
        ProblemDetail detail = createValidationProblemDetail(status, ex);
        log.debug("Validation errors: {}", detail);
        ResponseEntity<Object> response = handleExceptionInternal(
            ex, detail, headers, status, request);
        return response;
    }

    private ProblemDetail createValidationProblemDetail(
            HttpStatusCode status, Exception ex) {
        if (ex instanceof MethodArgumentNotValidException validEx) {
            List<String> validationErrors = validEx.getAllErrors().stream()
                .map(ObjectError::getDefaultMessage).toList();
            ProblemDetail problem = ProblemDetail
                .forStatusAndDetail(status, "Validation errors");
            problem.setProperty("validationErrors", validationErrors);
            return problem;
        }
        throw new IllegalArgumentException("Invalid exception type");
    }
}
