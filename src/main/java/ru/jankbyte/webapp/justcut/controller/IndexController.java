package ru.jankbyte.webapp.justcut.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.jankbyte.webapp.justcut.service.link.LinkShorterService;
import ru.jankbyte.webapp.justcut.model.link.Link;
import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class IndexController {
    private final LinkShorterService linkShorterService;

    @GetMapping("/redirect/{shortUrl}")
    public String redirect(@PathVariable("shortUrl") String shortUrl) {
        Link link = linkShorterService.getLinkByShortURL(shortUrl);
        String url = link.getUrl();
        return "redirect:%s".formatted(url);
    }
}
