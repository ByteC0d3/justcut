package ru.jankbyte.webapp.justcut.controller;

import java.util.List;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import jakarta.validation.Valid;
import ru.jankbyte.webapp.justcut.security.user.UserAccount;
import ru.jankbyte.webapp.justcut.service.link.LinkService;
import ru.jankbyte.webapp.justcut.service.link.LinkShorterService;
import ru.jankbyte.webapp.justcut.model.link.Link;
import ru.jankbyte.webapp.justcut.dto.link.LinkDto;
import ru.jankbyte.webapp.justcut.dto.link.NewLink;
import ru.jankbyte.webapp.justcut.dto.link.CreatedLink;
import ru.jankbyte.webapp.justcut.mapper.LinkMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/link", consumes = APPLICATION_JSON_VALUE)
public class LinkController {
    private final LinkService linkService;
    private final LinkShorterService linkShorterService;
    private final LinkMapper linkMapper;

    @PreAuthorize("""
        hasAnyAuthority('MANAGE_ALL_LINKS') ||
            (hasAuthority('MANAGE_OWN_LINKS') && principal.id == #id)
        """)
    @GetMapping("/getByPersonId/{id:\\d+}")
    public Iterable<LinkDto> getLinksByPersonId(@PathVariable("id") Long id) {
        List<Link> links = linkService.getByPersonId(id);
        return linkMapper.convertLinkToLinkDto(links);
    }

    @DeleteMapping("/deleteById/{id:\\d+}")
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("""
        hasAnyAuthority('MANAGE_ALL_LINKS') ||
          (hasAuthority('MANAGE_OWN_LINKS') &&
            @linkSecurityExpressions.isOwnerLink(#id, principal.id))
        """)
    public void deleteLinkById(@PathVariable("id") Long id) {
        linkService.removeById(id);
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('CREATE_LINKS')")
    public CreatedLink createNewLink(@RequestBody @Valid NewLink newLink,
            @AuthenticationPrincipal UserAccount account) {
        String original = newLink.url();
        long id = account.getId();
        String shorten = linkShorterService.createShortURL(original, id);
        return new CreatedLink(shorten);
    }
}
