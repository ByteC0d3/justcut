package ru.jankbyte.webapp.justcut.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api/person",
    consumes = APPLICATION_JSON_VALUE)
public class PersonController {
    @PreAuthorize("hasAnyAuthority('MANAGE_ACCOUNTS')")
    @DeleteMapping("/deleteById/{id:[\\d]+}")
    public void deleteById(@PathVariable("id") Long id) {

    }
}
