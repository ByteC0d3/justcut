package ru.jankbyte.webapp.justcut.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import ru.jankbyte.webapp.justcut.service.SignupService;
import ru.jankbyte.webapp.justcut.service.ConfirmationTokenService;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.dto.registration.NewPersonDto;
import ru.jankbyte.webapp.justcut.mapper.*;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;

import jakarta.validation.Valid;

@Tag(name = "Registration controller",
    description = """
        Provides operations for signup and confirms email""")
@RestController
@RequestMapping(value = {"/api/signup"},
    consumes = MediaType.APPLICATION_JSON_VALUE)
public class SignupController {
    private final SignupService registrationService;
    private final PersonMapper personMapper;
    private final ConfirmationTokenService confirmationService;
    private final Validator personEmailValidator;

    public SignupController(SignupService registrationService,
            PersonMapper personMapper,
            ConfirmationTokenService confirmationService,
            @Qualifier("personEmailExistsValidator")
                Validator personEmailValidator) {
        this.registrationService = registrationService;
        this.personMapper = personMapper;
        this.confirmationService = confirmationService;
        this.personEmailValidator = personEmailValidator;
    }

    @Operation(summary = "Signup new person",
        description = "Create new person account")
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createPerson(
            @Validated @Valid @RequestBody NewPersonDto newPersonDto) {
        Person person = personMapper.convertNewPersonDtoToPerson(
            newPersonDto);
        registrationService.signup(person);
    }

    @GetMapping("/confirm")
    public void confirmEmail(@RequestParam("token") String token) {
        confirmationService.verifyToken(token);
    }

    @InitBinder
    private void registerEmailValidator(WebDataBinder binder) {
        binder.addValidators(personEmailValidator);
    }
}
