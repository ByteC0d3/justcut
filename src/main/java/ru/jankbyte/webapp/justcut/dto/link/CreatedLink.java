package ru.jankbyte.webapp.justcut.dto.link;

/**
 * Contains response of created link.
 */
public record CreatedLink(String url) {
}
