package ru.jankbyte.webapp.justcut.dto.link;

import java.time.LocalDateTime;

/**
 * Contains response of some link.
 */
public record LinkDto(long id, long clicksCount,
    LocalDateTime created, String url, String shortUrl) {
}
