package ru.jankbyte.webapp.justcut.dto.link;

import ru.jankbyte.webapp.justcut.annotation.validation.IsURL;

/**
 * The DTO-request for creating new link by person.
 */
public record NewLink(@IsURL String url) {
}
