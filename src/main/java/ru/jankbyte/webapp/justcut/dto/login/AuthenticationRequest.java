package ru.jankbyte.webapp.justcut.dto.login;

import jakarta.validation.constraints.NotEmpty;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The DTO-request for authentication.
 */
public record AuthenticationRequest(
        @NotEmpty(message = "Username/email is empty")
        @Schema(description = "The username or email of account",
            example = "groovy", required = true) String username,
        @NotEmpty(message = "Password is empty")
        @Schema(description = "The password of account",
            example = "P@55w0rd", required = true) String password) {
}
