package ru.jankbyte.webapp.justcut.dto.login;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The DTO-response of success authentication.
 */
public record JwtAuthenticationResponse(
    @Schema(description = "Access token",
        example = "eyJhbGc.eyJzdWIiOiIxM.SflKxwRJSMeK") String accessToken,
    @Schema(description = "Refresh token",
        example = "jkhsd-jkhfdk-jsab") String refreshToken,
    @Schema(description = "Account ID",
        example = "100") Long accountId,
    @Schema(description = "Token type",
        example = "Bearer") String tokenType) {
}
