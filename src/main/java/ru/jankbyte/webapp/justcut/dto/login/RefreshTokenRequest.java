package ru.jankbyte.webapp.justcut.dto.login;

import jakarta.validation.constraints.NotEmpty;

/**
 * The DTO-request with refresh-token.
 */
public record RefreshTokenRequest(
    @NotEmpty(message = "Invalid refresh token") String refreshToken) {
}
