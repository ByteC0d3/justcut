/**
 * Contains packages with DTO (data-transfer-object) for controllers.
 */
package ru.jankbyte.webapp.justcut.dto;
