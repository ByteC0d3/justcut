package ru.jankbyte.webapp.justcut.dto.registration;

import lombok.Setter;
import lombok.Getter;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import java.util.Set;
import java.util.HashSet;

import jakarta.validation.constraints.NotEmpty;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Using for creating and returning account.
 */
@Setter
@Getter
@Schema(description = """
    The model for creating new managment account.""")
public class AccountDto {
    @Schema(description = "The username of account for login",
        example = "groovy", required = true)
    @NotEmpty(message = "Username is empty")
    private String name;

    @Schema(description = "The password of account",
        example = "Groovy123#", required = true)
    @NotEmpty(message = "Password is empty")
    private String password;

    @Schema(description = "The account authorities (roles)", required = true)
    @NotEmpty(message = "Authorities is empty")
    private Set<AuthorityType> authorities = new HashSet<>();
}
