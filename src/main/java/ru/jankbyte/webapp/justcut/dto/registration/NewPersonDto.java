package ru.jankbyte.webapp.justcut.dto.registration;

import jakarta.validation.constraints.NotEmpty;
import ru.jankbyte.webapp.justcut.annotation.validation.IsEmail;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Using for creating new person.
 */
@Schema(description = """
    The model for creating new person. \
    Used by customers when signup.""")
public record NewPersonDto(@IsEmail
    @Schema(description = "The email of person for login and verify",
        example = "groovy@gmail.com", required = true) String email,
    @NotEmpty(message = "Password is empty")
    @Schema(description = "The password of person",
        example = "Groovy123", required = true) String password) {
}
