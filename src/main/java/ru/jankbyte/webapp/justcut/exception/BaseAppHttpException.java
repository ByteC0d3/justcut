package ru.jankbyte.webapp.justcut.exception;

import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

/**
 * Base exception of application.
 */
public abstract class BaseAppHttpException extends ResponseStatusException {
    public BaseAppHttpException(HttpStatus status,
            String reason, Throwable cause) {
        super(status, reason, cause);
    }

    public BaseAppHttpException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
