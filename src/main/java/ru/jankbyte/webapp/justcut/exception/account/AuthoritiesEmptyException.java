package ru.jankbyte.webapp.justcut.exception.account;

import ru.jankbyte.webapp.justcut.exception.BaseAppHttpException;
import org.springframework.http.HttpStatus;

/**
 * Throws when creating account if authorities is empty
 */
public final class AuthoritiesEmptyException extends BaseAppHttpException {
    public AuthoritiesEmptyException() {
        super(HttpStatus.BAD_REQUEST,
            "Can't save account: authorities empty");
    }
}
