package ru.jankbyte.webapp.justcut.exception.notfound;

public final class AccountNotFoundException
        extends EntityNotFoundException {
    public AccountNotFoundException() {
        super("Account not found");
    }
}
