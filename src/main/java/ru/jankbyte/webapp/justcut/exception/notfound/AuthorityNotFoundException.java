package ru.jankbyte.webapp.justcut.exception.notfound;

public final class AuthorityNotFoundException
        extends EntityNotFoundException {
    public AuthorityNotFoundException() {
        super("Authority not found");
    }
}
