package ru.jankbyte.webapp.justcut.exception.notfound;

import ru.jankbyte.webapp.justcut.exception.BaseAppHttpException;
import org.springframework.http.HttpStatus;

public abstract class EntityNotFoundException extends BaseAppHttpException {
    public EntityNotFoundException(String reason) {
        super(HttpStatus.NOT_FOUND, reason);
    }
}
