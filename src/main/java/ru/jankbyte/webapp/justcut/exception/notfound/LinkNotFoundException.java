package ru.jankbyte.webapp.justcut.exception.notfound;

public final class LinkNotFoundException extends EntityNotFoundException {
    public LinkNotFoundException() {
        super("Link not found");
    }
}
