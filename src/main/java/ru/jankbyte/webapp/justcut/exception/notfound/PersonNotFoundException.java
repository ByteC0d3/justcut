package ru.jankbyte.webapp.justcut.exception.notfound;

public final class PersonNotFoundException extends EntityNotFoundException {
    public PersonNotFoundException() {
        super("Person not found");
    }
}
