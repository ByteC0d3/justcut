package ru.jankbyte.webapp.justcut.exception.notfound;

public final class RefreshTokenNotFoundException extends EntityNotFoundException {
    public RefreshTokenNotFoundException() {
        super("Refresh token not found");
    }
}
