package ru.jankbyte.webapp.justcut.exception.registration.token;

import ru.jankbyte.webapp.justcut.exception.BaseAppHttpException;
import org.springframework.http.HttpStatus;

public abstract class ConfirmationTokenException
        extends BaseAppHttpException {
    public ConfirmationTokenException(HttpStatus status, String message) {
        super(status, message);
    }
}
