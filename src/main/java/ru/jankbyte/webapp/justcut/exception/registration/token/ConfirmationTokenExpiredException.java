package ru.jankbyte.webapp.justcut.exception.registration.token;

import org.springframework.http.HttpStatus;

public final class ConfirmationTokenExpiredException
        extends ConfirmationTokenException {
    public ConfirmationTokenExpiredException() {
        super(HttpStatus.BAD_REQUEST,
            "Your token was expired. Please, register again");
    }
}
