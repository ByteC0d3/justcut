package ru.jankbyte.webapp.justcut.exception.registration.token;

import org.springframework.http.HttpStatus;

public final class InvalidConfirmationTokenException
        extends ConfirmationTokenException {
    public InvalidConfirmationTokenException() {
        super(HttpStatus.BAD_REQUEST, "Invalid conformation token");
    }
}
