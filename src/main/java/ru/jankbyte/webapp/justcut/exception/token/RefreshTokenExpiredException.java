package ru.jankbyte.webapp.justcut.exception.token;

import ru.jankbyte.webapp.justcut.exception.BaseAppHttpException;
import org.springframework.http.HttpStatus;

public class RefreshTokenExpiredException extends BaseAppHttpException {
    public RefreshTokenExpiredException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, "Refresh token expired");
    }
}