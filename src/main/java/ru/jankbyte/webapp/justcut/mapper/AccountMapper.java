package ru.jankbyte.webapp.justcut.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import ru.jankbyte.webapp.justcut.dto.registration.AccountDto;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.model.account.Authority;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;

import java.util.stream.Stream;
import java.util.Set;
import static java.util.stream.Collectors.toUnmodifiableSet;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {
    default Account convertAccountDtoToAccount(AccountDto dto) {
        Account account = new Account();
        account.setName(dto.getName());
        account.setPassword(dto.getPassword());
        Set<Authority> roles = dto.getAuthorities().stream()
            .map(Authority::of).collect(toUnmodifiableSet());
        account.setAuthorities(roles);
        return account;
    }

    default AccountDto convertAccountToAccountDto(Account account) {
        AccountDto dto = new AccountDto();
        dto.setName(account.getName());
        dto.setPassword(account.getPassword());
        Set<AuthorityType> authorities = account.getAuthorities()
            .stream().map(Authority::getName)
            .collect(toUnmodifiableSet());
        dto.setAuthorities(authorities);
        return dto;
    }
}
