package ru.jankbyte.webapp.justcut.mapper;

import ru.jankbyte.webapp.justcut.component.LongConversion;
import ru.jankbyte.webapp.justcut.model.link.Link;
import ru.jankbyte.webapp.justcut.dto.link.LinkDto;
import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;
import java.util.List;

@Component
@RequiredArgsConstructor
public class LinkMapper {
    private final LongConversion longConversion;

    public List<LinkDto> convertLinkToLinkDto(List<Link> links) {
        return links.stream().map(this::convertLinkToLinkDto)
            .toList();
    }

    public LinkDto convertLinkToLinkDto(Link link) {
        String shortUrl = longConversion.encode(link.getId());
        return new LinkDto(link.getId(), link.getClicksCount(),
            link.getCreated(), link.getUrl(), shortUrl);
    }
}
