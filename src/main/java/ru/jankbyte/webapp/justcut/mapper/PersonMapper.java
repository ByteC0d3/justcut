package ru.jankbyte.webapp.justcut.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import static org.mapstruct.ReportingPolicy.IGNORE;

import ru.jankbyte.webapp.justcut.dto.registration.NewPersonDto;
import ru.jankbyte.webapp.justcut.model.person.Person;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface PersonMapper {
    Person convertNewPersonDtoToPerson(NewPersonDto personDto);
}
