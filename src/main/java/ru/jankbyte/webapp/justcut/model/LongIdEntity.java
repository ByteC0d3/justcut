package ru.jankbyte.webapp.justcut.model;

import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import lombok.Setter;
import lombok.Getter;

@Setter
@Getter
@MappedSuperclass
public abstract class LongIdEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
}
