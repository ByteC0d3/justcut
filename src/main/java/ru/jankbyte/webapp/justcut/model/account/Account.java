package ru.jankbyte.webapp.justcut.model.account;

import jakarta.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.HashSet;

import ru.jankbyte.webapp.justcut.model.LongIdEntity;
import ru.jankbyte.webapp.justcut.model.person.Person;

import lombok.Builder;
import lombok.Setter;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * The managment-account representation (doesn't have email).
 */
@Getter
@Setter
@NamedEntityGraph(name = "Account.authentication",
    attributeNodes = @NamedAttributeNode("authorities"))
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "entity_type")
@Table(schema = "account_info", name = "account")
public class Account extends LongIdEntity {
    @Embedded
    private AccountTimes times;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "account_id")
    private Set<RefreshToken> refreshTokens = new HashSet<>();

    @ManyToMany(cascade = {
        CascadeType.DETACH, CascadeType.MERGE,
        CascadeType.PERSIST, CascadeType.REFRESH
    })
    @JoinTable(schema = "account_info",
        name = "account_authorities",
        joinColumns = @JoinColumn(name = "account_id"),
        inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authorities = new HashSet<>();

    @Column(length = 120, nullable = false, unique = true)
    private String name;

    @Column(length = 120, nullable = false)
    private String password;

    @Column(nullable = false)
    private boolean enabled;

    @Override
    public int hashCode() {
        return Objects.hash(name, password, times);
    }

    public void addAuthority(Authority authority) {
        authorities.add(authority);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (object instanceof Account account) {
            return Objects.equals(account.name, name) &&
                Objects.equals(account.enabled, enabled) &&
                Objects.equals(account.times, times) &&
                Objects.equals(account.password, password);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Account(id=%d, name=%s, password=%s, enabled=%b)"
            .formatted(id, name, password, enabled);
    }
}
