package ru.jankbyte.webapp.justcut.model.account;

import jakarta.persistence.*;
import ru.jankbyte.webapp.justcut.model.LongIdEntity;
import java.time.LocalDateTime;
import java.util.Objects;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Embeddable
public class AccountTimes {
    @Column(nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime registration;

    @Column(name = "latest_connection", columnDefinition = "TIMESTAMP")
    private LocalDateTime latestConnection;

    @Override
    public int hashCode() {
        return Objects.hash(registration, latestConnection);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (object instanceof AccountTimes times) {
            return Objects.equals(times.registration, registration) &&
                Objects.equals(times.latestConnection, latestConnection);
        }
        return false;
    }

    @Override
    public String toString() {
        return "AccountTimes(registration=%s, latestConnection=%s)"
            .formatted(registration.toString(), latestConnection.toString());
    }
}
