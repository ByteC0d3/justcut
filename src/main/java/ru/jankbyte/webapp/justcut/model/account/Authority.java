package ru.jankbyte.webapp.justcut.model.account;

import jakarta.persistence.*;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import org.springframework.security.core.GrantedAuthority;

import lombok.Setter;
import lombok.Getter;

import ru.jankbyte.webapp.justcut.model.LongIdEntity;

@Setter
@Getter
@Entity
@Table(schema = "account_info", name = "authority")
public class Authority extends LongIdEntity {
    @Column(nullable = false, length = 50,
        unique = true, updatable = false)
    @Enumerated(EnumType.STRING)
    private AuthorityType name;

    @Column(nullable = false, length = 180)
    private String description;

    @ManyToMany(mappedBy = "authorities")
    private Set<Account> authorities = new HashSet<>();

    public Authority() {}

    protected Authority(AuthorityType name) {
        this.name = name;
    }

    public static Authority of(AuthorityType name) {
        return new Authority(name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (object instanceof Authority authority) {
            return Objects.equals(authority.name, name) &&
                Objects.equals(authority.description, description);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Authority(id=%d, name=%s, description=%s)"
            .formatted(id, name, description);
    }

    public static enum AuthorityType {
        MANAGE_ACCOUNTS, MANAGE_OWN_LINKS,
        MANAGE_ALL_LINKS, CREATE_LINKS
    }
}
