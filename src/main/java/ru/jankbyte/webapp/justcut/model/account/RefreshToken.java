package ru.jankbyte.webapp.justcut.model.account;

import jakarta.persistence.Id;
import jakarta.persistence.Id;
import jakarta.persistence.GenerationType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Column;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;
import static jakarta.persistence.FetchType.LAZY;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Table;
import java.util.UUID;
import java.time.LocalDateTime;
import java.util.Objects;

import lombok.Builder;
import lombok.Setter;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NamedEntityGraph(name = "RefreshToken.account",
    attributeNodes = @NamedAttributeNode("account"))
@Entity
@Table(schema = "account_info", name = "refresh_token")
public class RefreshToken {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false, name = "expires_at",
        columnDefinition = "TIMESTAMP")
    private LocalDateTime expiresAt;

    @Column(nullable = false, name = "created_at",
        columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Override
    public int hashCode() {
        return Objects.hash(id, expiresAt, createdAt);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (object instanceof RefreshToken token) {
            return Objects.equals(token.expiresAt, expiresAt) &&
                Objects.equals(token.createdAt, createdAt);
        }
        return false;
    }

    @Override
    public String toString() {
        return "RefreshToken(id=%d, expiresAt=%s, createdAt=%s)"
            .formatted(id, expiresAt, createdAt);
    }
}
