package ru.jankbyte.webapp.justcut.model.link;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import ru.jankbyte.webapp.justcut.model.LongIdEntity;
import ru.jankbyte.webapp.justcut.model.person.Person;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NamedQuery(
    name = "Link.findByIdForIncressClicks",
    query = "SELECT l FROM Link l WHERE l.id = :id",
    lockMode = LockModeType.PESSIMISTIC_WRITE
)
@Entity
@Table(schema = "link_info", name = "link")
public class Link extends LongIdEntity {
    @Column(nullable = false, columnDefinition = "TEXT", updatable = false)
    private String url;

    @Column(nullable = false, name = "clicks_count")
    private long clicksCount;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime created;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable =  false)
    private Person person;

    @Override
    public String toString() {
        return "Link(id=%d, url=%s, created=%s, clicksCount=%d)"
            .formatted(id, url, created, clicksCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, created);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (object instanceof Link link) {
            return Objects.equals(url, link.url) &&
                Objects.equals(created, link.created);
        }
        return false;
    }

    public void incressClicks() {
        clicksCount += 1L;
    }
}
