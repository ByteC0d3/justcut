package ru.jankbyte.webapp.justcut.model.person;

import jakarta.persistence.*;
import java.util.Objects;
import java.time.LocalDateTime;
import ru.jankbyte.webapp.justcut.model.LongIdEntity;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@NamedEntityGraph(name = "ConfirmationToken.findByIdFetchPerson",
    attributeNodes = @NamedAttributeNode("person"))
@Table(schema = "account_info", name = "confirmation_token")
public class ConfirmationToken {
    @Id
    private Long id;

    @Column(columnDefinition = "TIMESTAMP",
        nullable = false, name = "created_at")
    private LocalDateTime createdAt;

    @Column(columnDefinition = "TIMESTAMP",
        nullable = false, name = "expires_at")
    private LocalDateTime expiresAt;

    @Column(columnDefinition = "TIMESTAMP", name = "confirmed_at")
    private LocalDateTime confirmedAt;

    @Column(nullable = false, unique = true)
    private String token;

    @MapsId
    @JoinColumn(name = "id")
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Person person;
}
