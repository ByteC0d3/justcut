package ru.jankbyte.webapp.justcut.model.person;

import jakarta.persistence.*;
import ru.jankbyte.webapp.justcut.model.LongIdEntity;
import ru.jankbyte.webapp.justcut.model.link.Link;
import ru.jankbyte.webapp.justcut.model.account.Account;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;

import lombok.*;

@Setter
@Getter
@Entity
public class Person extends Account {
    @Column(length = 120, nullable = false, unique = true)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "owner_id")
    private Set<Link> links = new HashSet<>();

    @Override
    public int hashCode() {
        return Objects.hash(email) + super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!super.equals(object)) return false;
        if (object instanceof Person person) {
            return Objects.equals(email, person.email);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Person(id=%d, email=%s)".formatted(id, email);
    }
}
