package ru.jankbyte.webapp.justcut.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.EntityGraph;

import ru.jankbyte.webapp.justcut.model.account.Account;
import java.util.Optional;

@Repository
public interface AccountRepository<T extends Account>
        extends JpaRepository<T,Long> {
    @Transactional(readOnly = true)
    @EntityGraph("Account.authentication")
    Optional<T> findByName(String name);
}
