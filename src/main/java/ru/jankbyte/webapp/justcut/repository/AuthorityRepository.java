package ru.jankbyte.webapp.justcut.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import ru.jankbyte.webapp.justcut.model.account.Authority;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;

import java.util.Optional;
import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority,Long> {
    @Transactional(readOnly = true)
    Optional<Authority> findByName(AuthorityType name);

    @Transactional(readOnly = true)
    List<Authority> findByNameIn(List<AuthorityType> names);
}
