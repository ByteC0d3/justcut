package ru.jankbyte.webapp.justcut.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
@Transactional
public interface ConfirmationTokenRepository
        extends JpaRepository<ConfirmationToken,Long> {
    @Transactional(readOnly = true)
    @EntityGraph("ConfirmationToken.findByIdFetchPerson")
    Optional<ConfirmationToken> findByToken(String token);

    void deleteByExpiresAtLessThanAndConfirmedAtIsNull(@Param("time") LocalDateTime time);
}
