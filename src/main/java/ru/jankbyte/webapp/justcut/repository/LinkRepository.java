package ru.jankbyte.webapp.justcut.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import jakarta.persistence.LockModeType;

import ru.jankbyte.webapp.justcut.model.link.Link;

import java.util.Optional;
import java.util.List;

@Repository
public interface LinkRepository extends JpaRepository<Link,Long> {
    @Transactional(readOnly = true)
    List<Link> findByPersonId(long id);

    @Modifying
    @Query("DELETE FROM Link l WHERE l.person.id = :id")
    @Transactional
    void deleteByPersonId(long id);

    @Transactional(propagation = Propagation.MANDATORY)
    @Query(name = "Link.findByIdForIncressClicks")
    Optional<Link> findByIdForIncressClicks(@Param("id") Long id);

    @Transactional(readOnly = true)
    boolean existsByPersonId(long id);

    @Query("""
        SELECT COUNT(l) > 0 FROM Link l
          INNER JOIN l.person p
            WHERE p.id = :personId AND l.id = :linkId
        """)
    @Transactional(readOnly = true)
    boolean existsInPerson(@Param("linkId") long linkId, @Param("personId") long personId);
}
