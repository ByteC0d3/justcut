package ru.jankbyte.webapp.justcut.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.transaction.annotation.Transactional;

import ru.jankbyte.webapp.justcut.model.person.Person;

import java.util.Optional;

@Repository
public interface PersonRepository extends AccountRepository<Person> {
    @Transactional(readOnly = true)
    @EntityGraph("Account.authentication")
    Optional<Person> findByEmail(String email);

    @Transactional(readOnly = true)
    boolean existsByEmail(String email);
}
