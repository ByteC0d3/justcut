package ru.jankbyte.webapp.justcut.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.jankbyte.webapp.justcut.model.account.RefreshToken;

import java.util.Optional;
import java.time.LocalDateTime;
import java.util.UUID;

@Repository
public interface RefreshTokenRepository
        extends JpaRepository<RefreshToken,UUID> {
    @EntityGraph("RefreshToken.account")
    @Transactional(readOnly = true)
    Optional<RefreshToken> findById(UUID id);

    @Modifying
    @Transactional
    @Query("""
        DELETE FROM RefreshToken t
            WHERE t.expiresAt <= :time
        """)
    void deleteAllByExpiresAtLessThan(@Param("time") LocalDateTime time);

    @Transactional(readOnly = true)
    boolean existsByAccountId(long id);
}
