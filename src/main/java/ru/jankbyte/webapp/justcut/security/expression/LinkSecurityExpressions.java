package ru.jankbyte.webapp.justcut.security.expression;

import org.springframework.stereotype.Component;
import ru.jankbyte.webapp.justcut.service.link.LinkService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Contains security-expressions for authorization.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class LinkSecurityExpressions {
    private final LinkService linkService;

    public boolean isOwnerLink(long linkId, long ownerId) {
        boolean isOwner = linkService.containsInPerson(linkId, ownerId);
        log.debug("Link (ID={}) contains into person (ID={}) = {}",
            linkId, ownerId, isOwner);
        return isOwner;
    }
}
