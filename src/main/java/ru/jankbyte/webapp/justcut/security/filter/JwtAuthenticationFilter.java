package ru.jankbyte.webapp.justcut.security.filter;

import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.util.StringUtils;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.stereotype.Component;

import org.springframework.web.servlet.HandlerExceptionResolver;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import ru.jankbyte.webapp.justcut.service.authentication.JwtService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

/**
 * Filter of JWT for authentication.
 */
@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    public static final String AUTHENTICATION_SCHEME_BEARER = "Bearer";

    private final UserDetailsChecker checker;
    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;
    private final AuthenticationSuccessHandler successHandler;
    private final AuthenticationEntryPoint entryPoint;

    public JwtAuthenticationFilter(JwtService jwtService,
            UserDetailsService userDetailsService,
            UserDetailsChecker checker,
            @Qualifier("successAuthenticationHandler")
                AuthenticationSuccessHandler successHandler,
            @Qualifier("securityExceptionsProcessor")
                AuthenticationEntryPoint entryPoint) {
        this.jwtService = jwtService;
        this.userDetailsService = userDetailsService;
        this.successHandler = successHandler;
        this.entryPoint = entryPoint;
        this.checker = checker;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
                throws IOException, ServletException {
        try {
            String jwt = extractTokenFromRequest(request);
            if (jwt != null) {
                Authentication auth = authenticateRequest(jwt, request);
                onSuccessfulAuthentication(request, response, auth);
                log.debug("Authenticated like: {}", auth.getPrincipal());
            }
        } catch (AuthenticationException ex) {
            log.debug("Authentication problem: {}", ex.getMessage());
            entryPoint.commence(request, response, ex);
            return;
        }
        filterChain.doFilter(request, response);
    }

    protected void onSuccessfulAuthentication(HttpServletRequest request,
            HttpServletResponse response, Authentication authResult)
                throws IOException, ServletException {
        successHandler.onAuthenticationSuccess(
            request, response, authResult);
    }

    private Authentication authenticateRequest(String jwt,
            HttpServletRequest request) {
        try {
            Authentication authentication = null;
            String username = jwtService
                .extractClaim(jwt, Claims::getSubject);
            if (username != null && SecurityContextHolder.getContext()
                    .getAuthentication() == null) {
                UserDetails user = userDetailsService
                    .loadUserByUsername(username);
                checker.check(user);
                authentication = getToken(user, request);
                SecurityContextHolder.getContext()
                    .setAuthentication(authentication);
            }
            return authentication;
        } catch (JwtException exp) {
            throw new BadCredentialsException("Invalid token", exp);
        }
    }

    private Authentication getToken(UserDetails user,
            HttpServletRequest request) {
        UsernamePasswordAuthenticationToken userToken =
            UsernamePasswordAuthenticationToken.authenticated(
                user, null, user.getAuthorities());
        WebAuthenticationDetails authDetails =
            new WebAuthenticationDetailsSource()
                .buildDetails(request);
        userToken.setDetails(authDetails);
        return userToken;
    }

    private String extractTokenFromRequest(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header == null) {
            return null;
        }
        header = header.trim();
        if (!StringUtils.startsWithIgnoreCase(
                header, AUTHENTICATION_SCHEME_BEARER)) {
            return null;
        }
		if (header.equalsIgnoreCase(AUTHENTICATION_SCHEME_BEARER)) {
			throw new BadCredentialsException(
                "Empty bearer authentication token");
		}
        String token = header.substring(
            AUTHENTICATION_SCHEME_BEARER.length() + 1);
        return token;
    }
}
