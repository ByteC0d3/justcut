package ru.jankbyte.webapp.justcut.security.filter;

import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.AuthenticationException;
import org.springframework.http.ProblemDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.server.ServletServerHttpRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Endpoint of exceptions in authentication/authorization.
 * <p>Executes from {@link ru.jankbyte.webapp.justcut.security.SecurityConfig#apiSecuredFilterChain}
 * and {@link ru.jankbyte.webapp.justcut.security.filter.JwtAuthenticationFilter#doFilterInternal} (on failure).</p>
 * <p>Generates error-responses in RFC-7807 format.</p>
 * @see AuthenticationEntryPoint
 * @see AccessDeniedHandler
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class SecurityExceptionsProcessor
        implements AuthenticationEntryPoint, AccessDeniedHandler {
    private final ObjectMapper mapper;

    /**
     * Processing authentication errors.
     * @param request The client request
     * @param response The server response
     * @param authenticationException The exception with information
     */
    @Override
    public void commence(HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authenticationException)
                throws IOException, ServletException {
        processErrorRequest(request, response, authenticationException);
    }

    /**
     * Processing authorization (role-based) errors.
     * @param request The client request
     * @param response The server response
     * @param accessDeniedException The exception with information
     */
    @Override
    public void handle(HttpServletRequest request,
            HttpServletResponse response,
            AccessDeniedException accessDeniedException)
                throws IOException, ServletException {
        processErrorRequest(request, response, accessDeniedException);
    }

    private void processErrorRequest(HttpServletRequest request,
            HttpServletResponse response, Exception securityException)
                throws IOException, ServletException {
        ProblemDetail detail = createProblemDetail(securityException, request);
        response.setStatus(detail.getStatus());
        log.debug("Handling authentication exception ({} | {})",
            securityException.getClass().getName(), detail.getStatus());
        OutputStream os = response.getOutputStream();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        mapper.writeValue(os, detail);
        os.flush();
    }

    private ProblemDetail createProblemDetail(
            Exception exception, HttpServletRequest request) {
        int status = (exception instanceof AccessDeniedException) ?
            HttpServletResponse.SC_FORBIDDEN :
            HttpServletResponse.SC_UNAUTHORIZED;
        ProblemDetail detail = ProblemDetail.forStatus(status);
        detail.setDetail(exception.getMessage());
        try {
            String servletPath = request.getServletPath();
            URI uri = new URI(servletPath);
            detail.setInstance(uri);
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
        log.debug("Problem details of authentication: {}", detail);
        return detail;
    }
}
