package ru.jankbyte.webapp.justcut.security.filter;

import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.security.core.Authentication;


import ru.jankbyte.webapp.justcut.security.user.UserAccount;
import ru.jankbyte.webapp.justcut.service.AccountService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class SuccessAuthenticationHandler
        implements AuthenticationSuccessHandler {
    private final AccountService accountService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
            HttpServletResponse response, Authentication authentication)
                throws ServletException, IOException {
        Long id = ((UserAccount) authentication.getPrincipal()).getId();
        log.debug("Updading latest connection of account with ID={}", id);
        accountService.updateLatestConnection(id);
    }
}