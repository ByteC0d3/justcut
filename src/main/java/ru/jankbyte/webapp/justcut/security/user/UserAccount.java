package ru.jankbyte.webapp.justcut.security.user;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.stream.Collectors;

import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.model.account.Authority;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;

/**
 * The representation of principal in authentication.
 */
public class UserAccount implements UserDetails {
    private final Account account;
    private final Collection<? extends GrantedAuthority> authorities;

    public UserAccount(Account account) {
        this.account = account;
        authorities = account.getAuthorities().stream()
            .map(authority -> new SimpleGrantedAuthority(
                authority.getName().name())).toList();
    }

    public Long getId() {
        return account.getId();
    }

    public boolean isPersonAccount() {
        return account instanceof Person;
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return account.isEnabled();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String toString() {
        Collection<String> authoritiesNames = authorities.stream()
            .map(GrantedAuthority::getAuthority).toList();
        return """
            UserAccount[id=%d, isPerson=%b, \
            username=%s, enabled=%b, authorities=%s]"""
                .formatted(getId(), isPersonAccount(), getUsername(),
                    isEnabled(), authoritiesNames);
    }
}
