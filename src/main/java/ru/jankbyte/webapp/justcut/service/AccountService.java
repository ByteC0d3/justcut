package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.account.Account;
import org.springframework.security.provisioning.UserDetailsManager;

public interface AccountService {
    /**
     * Create new account.
     * <p>If account name - is null, then name will generating
     * by database like 'user-<current-table-sequence>'.</p>
     * @param account The data of account
     * @return Same account, but with generated ID
     */
    Account createAccount(Account account);

    /**
     * Getting account by name.
     * @param name The name of account
     * @return Account with this name
     */
    Account getByName(String name);

    /**
     * Getting account by id.
     * @param id The account ID
     * @return The account with this ID
     */
    Account getById(long id);

    /**
     * Update existing account.
     * @param id The target account ID
     * @return The same account with updated properties
     */
    void updateLatestConnection(long id);

    /**
     * Delete account by id.
     * @param id The ID of account
     */
    void removeById(long id);
}
