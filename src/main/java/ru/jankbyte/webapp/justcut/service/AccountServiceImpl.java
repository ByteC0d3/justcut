package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.account.*;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import ru.jankbyte.webapp.justcut.repository.AccountRepository;
import ru.jankbyte.webapp.justcut.service.AccountService;
import ru.jankbyte.webapp.justcut.service.AuthorityService;
import ru.jankbyte.webapp.justcut.service.PersonService;
import ru.jankbyte.webapp.justcut.exception.account.AuthoritiesEmptyException;
import ru.jankbyte.webapp.justcut.exception.notfound.AccountNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.function.Supplier;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository<Account> repository;
    private final AuthorityService authorityService;
    private final PasswordEncoder encoder;

    private final Supplier<AccountNotFoundException> accountNotFoundThrowing =
        AccountNotFoundException::new;

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Account createAccount(Account account) {
        decorateAccount(account);
        account = repository.save(account);
        log.debug("Refreshing saved account");
        // NOTE: refresh account from database, because
        // if account has not contains name - that's set in database.
        em.refresh(account);
        return account;
    }

    @Override
    @Transactional
    public void updateLatestConnection(long id) {
        Account account = getById(id);
        LocalDateTime now = LocalDateTime.now();
        account.getTimes().setLatestConnection(now);
        repository.save(account);
    }

    @Override
    public Account getById(long id) {
        return repository.findById(id)
            .orElseThrow(accountNotFoundThrowing);
    }

    @Override
    public Account getByName(String name) {
        return repository.findByName(name)
            .orElseThrow(accountNotFoundThrowing);
    }

    @Override
    public void removeById(long id) {
        repository.deleteById(id);
    }

    /**
     * Decorating account and preparing for saving
     * into database.
     * <p>For example: we need encode password before storing
     * into database, and also need "reload" authorities from database.</p>
     * @throws AuthoritiesEmptyException if account has not any authority
     */
    private void decorateAccount(Account account) {
        setAccountAuthorties(account);
        LocalDateTime now = LocalDateTime.now();
        AccountTimes times = AccountTimes.builder()
            .registration(now).build();
        account.setTimes(times);
        String originalPassword = account.getPassword();
        String encodedPassword = encoder.encode(originalPassword);
        account.setPassword(encodedPassword);
    }

    private void setAccountAuthorties(Account account) {
        Set<Authority> authorities = account.getAuthorities();
        if (authorities.isEmpty()) {
            throw new AuthoritiesEmptyException();
        }
        AuthorityType[] authorityNames = authorities.stream()
            .map(Authority::getName).toArray(AuthorityType[]::new);
        authorities = authorityService.getByNames(authorityNames);
        account.setAuthorities(authorities);
    }
}
