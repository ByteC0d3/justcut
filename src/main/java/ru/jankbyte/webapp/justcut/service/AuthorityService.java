package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import ru.jankbyte.webapp.justcut.model.account.Authority;
import java.util.Set;

public interface AuthorityService {
    /**
     * Getting authority by name
     * @param name The name of authority
     * @return Authority with this name
     */
    Authority getByName(AuthorityType name);

    /**
     * Getting list of authorites.
     * @param authorities The names
     * @return The collection of authorities
     */
    Set<Authority> getByNames(AuthorityType... authorities);
}
