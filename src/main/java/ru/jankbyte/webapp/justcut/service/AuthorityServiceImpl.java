package ru.jankbyte.webapp.justcut.service;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;
import ru.jankbyte.webapp.justcut.model.account.Authority;
import ru.jankbyte.webapp.justcut.repository.AuthorityRepository;
import ru.jankbyte.webapp.justcut.service.AuthorityService;
import ru.jankbyte.webapp.justcut.exception.notfound.AuthorityNotFoundException;

@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository repository;

    @Override
    public Authority getByName(AuthorityType name) {
        return repository.findByName(name)
            .orElseThrow(AuthorityNotFoundException::new);
    }

    @Override
    public Set<Authority> getByNames(AuthorityType... authorityNames) {
        List<AuthorityType> authorityNamesList =
            Arrays.asList(authorityNames);
        List<Authority> authoritiesList = repository
            .findByNameIn(authorityNamesList);
        return new HashSet<Authority>(authoritiesList);
    }
}
