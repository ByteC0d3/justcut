package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;
import ru.jankbyte.webapp.justcut.model.person.Person;

/**
 * Service for control tokens for confirming person by email.
 */
public interface ConfirmationTokenService {
    /**
     * Verifing token.
     */
    void verifyToken(String token);

    /**
     * Save or update confirmation token.
     * @param confirmationToken The token
     * @return Saved confirmation token
     */
    ConfirmationToken saveOrUpdate(ConfirmationToken confirmationToken);

    /**
     * Create and save confirmation token for person.
     * @param person The target person
     * @return The saved confirmation token
     */
    ConfirmationToken createForPerson(Person person);

    /**
     * Remove expired confirmation tokens.
     */
    void removeExpiredConfirmationTokens();
}
