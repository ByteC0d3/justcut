package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.exception.registration.token.InvalidConfirmationTokenException;
import ru.jankbyte.webapp.justcut.repository.ConfirmationTokenRepository;
import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.config.property.EmailConfirmationProperties;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.time.LocalDateTime;
import static java.time.temporal.ChronoUnit.MILLIS;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl
        implements ConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final EmailConfirmationProperties emailConfirmationConfig;
    private final ConfirmationTokenValidationService tokenValidationService;

    @Override
    public ConfirmationToken createForPerson(Person person) {
        ConfirmationToken token = buildCofirmationToken(person);
        return confirmationTokenRepository.save(token);
    }

    @Override
    public ConfirmationToken saveOrUpdate(
            ConfirmationToken confirmationToken) {
        return confirmationTokenRepository.save(confirmationToken);
    }

    @Override
    @Transactional
    public void verifyToken(String token) {
        ConfirmationToken confToken =
            confirmationTokenRepository.findByToken(token)
                .orElseThrow(InvalidConfirmationTokenException::new);
        tokenValidationService.validateToken(confToken);
        LocalDateTime now = LocalDateTime.now();
        confToken.setConfirmedAt(now);
        Person account = confToken.getPerson();
        account.setEnabled(true);
        confirmationTokenRepository.save(confToken);
    }

    @Override
    public void removeExpiredConfirmationTokens() {
        LocalDateTime now = LocalDateTime.now();
        confirmationTokenRepository.deleteByExpiresAtLessThanAndConfirmedAtIsNull(now);
    }

    private ConfirmationToken buildCofirmationToken(Person person) {
        LocalDateTime createdAt = LocalDateTime.now();
        long milliseconds = emailConfirmationConfig.timeExpiring()
            .getTimeInMilliseconds();
        LocalDateTime expiresAt = createdAt.plus(milliseconds, MILLIS);
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = ConfirmationToken.builder()
            .createdAt(createdAt).expiresAt(expiresAt)
            .person(person).token(token).build();
        return confirmationToken;
    }
}
