package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;

/**
 * Service for validating confirmation token.
 */
public interface ConfirmationTokenValidationService {
    /**
     * Checking for expiring token.
     * @param token The confirmation token for checking
     * @return True - if token expired
     */
    boolean isTokenExpired(ConfirmationToken token);

    /**
     * Checking for activating token.
     * @param token The confirmation token for checking
     * @return True - if token was activated
     */
    boolean isTokenActivated(ConfirmationToken token);

    /**
     * Checking expiring and activating of token.
     * <p>That's a combination of two other methods: isTokenActivated and isTokenExpired.</p>
     * @param token The confirmation token for checking
     * @throws ConfirmationTokenExpiredException if token expired
     * @throws ConfirmationTokenActivatedException if token was activated
     */
    void validateToken(ConfirmationToken token);
}
