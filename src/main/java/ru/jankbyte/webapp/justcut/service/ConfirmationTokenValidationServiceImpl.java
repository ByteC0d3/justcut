package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;
import ru.jankbyte.webapp.justcut.exception.registration.token.ConfirmationTokenExpiredException;
import ru.jankbyte.webapp.justcut.exception.registration.token.ConfirmationTokenActivatedException;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;

@Service
public class ConfirmationTokenValidationServiceImpl
        implements ConfirmationTokenValidationService {
    @Override
    public boolean isTokenExpired(ConfirmationToken token) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiration = token.getExpiresAt();
        return now.isAfter(expiration);
    }

    @Override
    public boolean isTokenActivated(ConfirmationToken token) {
        return token.getConfirmedAt() != null;
    }

    @Override
    public void validateToken(ConfirmationToken token) {
        boolean isActivated = isTokenActivated(token);
        boolean isExpired = isTokenExpired(token);
        if (isActivated) {
            throw new ConfirmationTokenActivatedException();
        } else if (isExpired) {
            throw new ConfirmationTokenExpiredException();
        }
    }
}
