package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.person.Person;

public interface PersonService {
    /**
     * Create new person.
     * @param person The person data
     * @return Same person, but with generated ID
     */
    Person saveOrUpdate(Person person);

    /**
     * Getting person by email.
     * @param email The email
     * @return Person with this email
     */
    Person getByEmail(String email);

    /**
     * Getting person by id.
     * @param The unique ID
     * @return person with this ID
     */
    Person getById(Long id);

    /**
     * Removing person by ID.
     * @param id The person ID
     */
    void removeById(long id);

    /**
     * Checking exists by email.
     * @param email The person email
     * @return True - if person with this email exists
     */
    boolean existsByEmail(String email);
}
