package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.repository.PersonRepository;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.service.PersonService;
import ru.jankbyte.webapp.justcut.service.link.LinkService;
import ru.jankbyte.webapp.justcut.service.AccountService;
import ru.jankbyte.webapp.justcut.exception.notfound.PersonNotFoundException;

import java.util.function.Supplier;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepository repository;
    private final LinkService linkService;
    private final AccountService accountService;

    private final Supplier<PersonNotFoundException> personNotFoundThrowing =
        PersonNotFoundException::new;

    @Override
    public Person saveOrUpdate(Person person) {
        return repository.save(person);
    }

    @Override
    public boolean existsByEmail(String email) {
        return repository.existsByEmail(email);
    }

    @Override
    public Person getByEmail(String email) {
        return repository.findByEmail(email)
            .orElseThrow(personNotFoundThrowing);
    }

    @Override
    public Person getById(Long id) {
        return repository.findById(id)
            .orElseThrow(personNotFoundThrowing);
    }

    @Override
    @Transactional
    public void removeById(long id) {
        linkService.removeByPersonId(id);
        repository.flush();
        repository.deleteById(id);
    }
}
