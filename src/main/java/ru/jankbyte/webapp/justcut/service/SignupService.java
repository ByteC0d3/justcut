package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;

/**
 * The registration service.
 */
public interface SignupService {
    /**
     * Signup new person.
     * @param person Person data (email, password...)
     * @return Same person but with generated ID
     */
    Person signup(Person person);
}
