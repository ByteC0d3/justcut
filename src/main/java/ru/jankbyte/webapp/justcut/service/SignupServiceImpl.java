package ru.jankbyte.webapp.justcut.service;

import ru.jankbyte.webapp.justcut.model.account.*;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.model.person.ConfirmationToken;
import ru.jankbyte.webapp.justcut.service.ConfirmationTokenService;
import ru.jankbyte.webapp.justcut.service.SignupService;
import ru.jankbyte.webapp.justcut.service.AccountService;
import ru.jankbyte.webapp.justcut.service.AuthorityService;
import ru.jankbyte.webapp.justcut.service.PersonService;
import static ru.jankbyte.webapp.justcut.model.account.Authority.AuthorityType;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import java.util.Set;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@RequiredArgsConstructor
public class SignupServiceImpl implements SignupService {
    private final AccountService accountService;
    private final PersonService personService;
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    @Transactional
    public Person signup(Person person) {
        log.debug("Trying singup new person");
        setAccountAuthorities(person);
        person = (Person) accountService.createAccount(person);
        log.debug("Creating confirmation token for person");
        ConfirmationToken token = confirmationTokenService
            .createForPerson(person);
        return token.getPerson();
    }

    private void setAccountAuthorities(Account account) {
        Set.of(Authority.of(AuthorityType.MANAGE_OWN_LINKS),
            Authority.of(AuthorityType.CREATE_LINKS)
        ).forEach(account::addAuthority);
    }
}
