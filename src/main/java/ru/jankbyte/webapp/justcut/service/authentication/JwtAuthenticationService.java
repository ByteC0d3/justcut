package ru.jankbyte.webapp.justcut.service.authentication;

import ru.jankbyte.webapp.justcut.dto.login.*;

/**
 * The authentication service that's provide
 * methods of JWT-authentication.
 */
public interface JwtAuthenticationService {
    /**
     * Create token-pair (jwt + refresh) for account.
     * @param request The authentication request with username and password
     * @return The response with new pair of tokens
     * @throws AuthenticationException if some problems with credentials
     */
    JwtAuthenticationResponse createTokens(
        AuthenticationRequest request);

    /**
     * Refresh expired JWT by refresh token.
     * @param refreshTokenRequest The entity with refresh token
     * @return The response with new pair of tokens
     * @throws AuthenticationException if some problems with credentials
     */
    JwtAuthenticationResponse refreshTokens(
            RefreshTokenRequest refreshTokenRequest);
}
