package ru.jankbyte.webapp.justcut.service.authentication;

import org.springframework.stereotype.Service;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.transaction.annotation.Transactional;

import ru.jankbyte.webapp.justcut.model.account.RefreshToken;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.security.user.UserAccount;
import ru.jankbyte.webapp.justcut.dto.login.*;
import ru.jankbyte.webapp.justcut.service.AccountService;
import ru.jankbyte.webapp.justcut.service.authentication.refreshtoken.RefreshTokenServiceFacade;
import ru.jankbyte.webapp.justcut.service.authentication.refreshtoken.RefreshTokenService;
import static ru.jankbyte.webapp.justcut.security.filter.JwtAuthenticationFilter.AUTHENTICATION_SCHEME_BEARER;

import java.util.UUID;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtAuthenticationServiceImpl
        implements JwtAuthenticationService {
    private final UserDetailsChecker checker;
    private final AuthenticationManager authManager;
    private final RefreshTokenServiceFacade refreshTokenServiceFacade;
    private final RefreshTokenService refreshTokenService;
    private final JwtService jwtService;
    private final UserDetailsService userService;

    @Override
    @Transactional
    public JwtAuthenticationResponse createTokens(
            AuthenticationRequest request) {
        log.debug("Trying to generate JWT for credentials: {}", request);
        String name = request.username();
        String password = request.password();
        UserAccount user = (UserAccount) authenticate(name, password)
            .getPrincipal();
        return generateToken(user);
    }

    @Override
    @Transactional
    public JwtAuthenticationResponse refreshTokens(
            RefreshTokenRequest refreshTokenRequest) {
        String uuidToken = refreshTokenRequest.refreshToken();
        UUID id = UUID.fromString(uuidToken);
        RefreshToken refreshToken = refreshTokenService.getById(id);
        Account account = refreshToken.getAccount();
        String username = account.getName();
        refreshTokenService.removeById(id);
        refreshTokenService.verifyToken(refreshToken);
        UserAccount user = (UserAccount) userService
            .loadUserByUsername(username);
        checker.check(user);
        return generateToken(user);
    }

    private JwtAuthenticationResponse generateToken(UserAccount user) {
        Long userId = user.getId();
        RefreshToken refreshToken = refreshTokenServiceFacade
            .createTokenByAccountId(userId);
        String refreshTokenId = refreshToken.getId().toString();
        String jwt = jwtService.generateToken(user);
        return new JwtAuthenticationResponse(jwt, refreshTokenId, userId,
            AUTHENTICATION_SCHEME_BEARER);
    }

    private Authentication authenticate(String username, String password) {
        UsernamePasswordAuthenticationToken token =
            UsernamePasswordAuthenticationToken
                .unauthenticated(username, password);
        return authManager.authenticate(token);
    }
}
