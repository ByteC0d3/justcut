package ru.jankbyte.webapp.justcut.service.authentication;

import io.jsonwebtoken.Claims;

import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Provides operations for working with JWT
 * authentication token.
 */
public interface JwtService {
    /**
     * Extracting claims of JWT.
     * @param jwt The token
     * @return The body of JWT with data
     */
    Claims extractClaims(String jwt);
    <T> T extractClaim(String token, Function<Claims,T> claimsResolver);
    String generateToken(UserDetails userDetails);
}
