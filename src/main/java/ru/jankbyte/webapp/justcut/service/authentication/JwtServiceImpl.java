package ru.jankbyte.webapp.justcut.service.authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import static io.jsonwebtoken.security.Keys.hmacShaKeyFor;
import java.util.Base64;
import java.util.function.Function;
import java.util.Date;
import java.security.Key;
import lombok.RequiredArgsConstructor;
import ru.jankbyte.webapp.justcut.config.property.JwtTokenProperties;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetails;

@Service
public class JwtServiceImpl implements JwtService {
    private final JwtTokenProperties jwtProps;
    private final Key key;

    public JwtServiceImpl(JwtTokenProperties jwtProps) {
        this.jwtProps = jwtProps;
        this.key = getSecretKey();
    }

    @Override
    public String generateToken(UserDetails userDetails) {
        long current = System.currentTimeMillis();
        Date now = new Date(current);
        Date expiries = new Date(current + jwtProps.getTimeInMilliseconds());
        return Jwts.builder().signWith(key)
            .setSubject(userDetails.getUsername())
            .setExpiration(expiries)
            .setIssuedAt(now).compact();
    }

    @Override
    public Claims extractClaims(String jwt) {
        return Jwts.parserBuilder()
            .setSigningKey(key)
            .build().parseClaimsJws(jwt).getBody();
    }

    @Override
    public <T> T extractClaim(String token,
            Function<Claims,T> claimsResolver) {
        Claims claims = extractClaims(token);
        return claimsResolver.apply(claims);
    }

    private Key getSecretKey() {
        byte[] decoded = Base64.getDecoder()
            .decode(jwtProps.getSecret().getBytes());
        return hmacShaKeyFor(decoded);
    }
}
