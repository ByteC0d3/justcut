package ru.jankbyte.webapp.justcut.service.authentication;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import ru.jankbyte.webapp.justcut.exception.notfound.EntityNotFoundException;
import ru.jankbyte.webapp.justcut.security.user.UserAccount;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.service.AccountService;
import ru.jankbyte.webapp.justcut.service.PersonService;
import static ru.jankbyte.webapp.justcut.util.RegExUtils.isEmail;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final AccountService accountService;
    private final PersonService personService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        try {
            boolean isEmail = isEmail(username);
            Account account = isEmail ?
                personService.getByEmail(username) :
                accountService.getByName(username);
            UserDetails userDetails = new UserAccount(account);
            log.debug("Trying authorize account: {}", userDetails);
            return userDetails;
        } catch (EntityNotFoundException exp) {
            String exceptionMessage = exp.getMessage();
            throw new UsernameNotFoundException(exceptionMessage);
        }
    }
}
