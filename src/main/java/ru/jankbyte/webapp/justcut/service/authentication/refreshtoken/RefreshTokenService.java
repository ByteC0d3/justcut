package ru.jankbyte.webapp.justcut.service.authentication.refreshtoken;

import ru.jankbyte.webapp.justcut.model.account.RefreshToken;
import ru.jankbyte.webapp.justcut.model.account.Account;
import java.util.UUID;

/**
 * Provides opeations under refresh tokens.
 */
public interface RefreshTokenService {
    /**
     * Create new refresh token for account.
     * @param account The target account
     * @return New refresh-token
     */
    RefreshToken createTokenForAccount(Account account);

    /**
     * Getting refresh token by ID.
     * @param id The ID in UUID standart
     * @return The refresh token
     * @throws RefreshTokenNotFoundException if token not found
     */
    RefreshToken getById(UUID id);

    /**
     * Remove refresh token by ID.
     * @param id The ID in UUID standart
     */
    void removeById(UUID id);

    /**
     * Verify refresh token.
     * <p>Main responsibility of this method - checking expiring of token.</p>
     * @param token The target token
     * @throws RefreshTokenExpiredException if token expired
     */
    void verifyToken(RefreshToken token);

    /**
     * Remove all expired refresh tokens.
     */
    void removeExpiredTokens();
}
