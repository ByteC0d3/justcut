package ru.jankbyte.webapp.justcut.service.authentication.refreshtoken;

import ru.jankbyte.webapp.justcut.model.account.RefreshToken;

/**
 * Provides operations under token combining with other services. 
 */
public interface RefreshTokenServiceFacade {
    /**
     * Create refresh token by account ID.
     * @param accountId The account ID
     * @return The new generated refresh token
     */
    RefreshToken createTokenByAccountId(Long accountId);
}