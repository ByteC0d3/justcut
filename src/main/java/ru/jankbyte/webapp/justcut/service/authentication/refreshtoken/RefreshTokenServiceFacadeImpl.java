package ru.jankbyte.webapp.justcut.service.authentication.refreshtoken;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jankbyte.webapp.justcut.model.account.RefreshToken;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.service.AccountService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceFacadeImpl
        implements RefreshTokenServiceFacade {
    private final RefreshTokenService refreshTokenService;
    private final AccountService accountService;

    @Transactional
    public RefreshToken createTokenByAccountId(Long accountId) {
        Account account = accountService.getById(accountId);
        RefreshToken refreshToken = refreshTokenService
            .createTokenForAccount(account);
        return refreshToken;
    }
}
