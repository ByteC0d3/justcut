package ru.jankbyte.webapp.justcut.service.authentication.refreshtoken;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;
import ru.jankbyte.webapp.justcut.model.account.RefreshToken;
import ru.jankbyte.webapp.justcut.model.account.Account;
import ru.jankbyte.webapp.justcut.repository.RefreshTokenRepository;
import ru.jankbyte.webapp.justcut.exception.notfound.RefreshTokenNotFoundException;
import ru.jankbyte.webapp.justcut.exception.token.RefreshTokenExpiredException;
import ru.jankbyte.webapp.justcut.config.property.RefreshTokenProperties;

import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {
    private final RefreshTokenRepository refreshTokenRepository;
    private final RefreshTokenProperties properties;

    @Override
    @Transactional
    public RefreshToken createTokenForAccount(Account account) {
        log.debug("Generating refresh token ({} {}) for account: {}",
            properties.getExpiresAfter(), properties.getTimeUnit(),
                account.toString());
        LocalDateTime now = LocalDateTime.now();
        long milliseconds = properties.getTimeInMilliseconds();
        LocalDateTime expiresAt = now.plus(milliseconds, ChronoUnit.MILLIS);
        RefreshToken refreshToken = RefreshToken.builder()
            .account(account).expiresAt(expiresAt)
            .createdAt(now).build();
        return refreshTokenRepository.save(refreshToken);
    }

    @Override
    public void removeExpiredTokens() {
        LocalDateTime current = LocalDateTime.now();
        refreshTokenRepository.deleteAllByExpiresAtLessThan(current);
    }

    @Override
    public void verifyToken(RefreshToken refreshToken) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiresAt = refreshToken.getExpiresAt();
        if (expiresAt.isBefore(now)) {
            log.debug("Token expired: {} ({} > {})",
                refreshToken.getId(), now, expiresAt);
            throw new RefreshTokenExpiredException();
        }
    }

    @Override
    public RefreshToken getById(UUID id) {
        return refreshTokenRepository.findById(id)
            .orElseThrow(RefreshTokenNotFoundException::new);
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public void removeById(UUID id) {
        refreshTokenRepository.deleteById(id);
    }
}
