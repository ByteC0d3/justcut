package ru.jankbyte.webapp.justcut.service.link;

import ru.jankbyte.webapp.justcut.model.link.Link;
import org.springframework.security.access.prepost.PreAuthorize;
import java.util.List;

public interface LinkService {
    /**
     * Getting link by ID.
     * @param id The link ID
     * @return The link with target ID
     */
    Link getById(long id);

    /**
     * Checking is contains link in person.
     * @param linkId The link ID for checking
     * @param personId The person ID that collection need checking
     * @return True - if contains
     */
    boolean containsInPerson(long linkId, long personId);

    /**
     * Getting link by ID for clicks increment.
     * <p>This method using pessimistic read lock for database-row</p>
     * @param id The link ID
     * @return The link with target ID.
     */
    Link getByIdForIncressClicks(Long id);
    /**
     * Getting links by person id
     * @param id The id of person
     * @return The list with links of person
     */
    List<Link> getByPersonId(long id);

    /**
     * Getting count of links.
     * @return The count of links
     */
    long getCount();

    /**
     * Adding new link for person.
     * @param link The link data for creating
     * @param personId The owner id
     * @return The new link
     */
    Link createLink(Link link, long personId);

    /**
     * Save or update link.
     * @param link The link
     * @return The link with updated data (or inserted id)
     */
    Link saveOrUpdate(Link link);

    /**
     * Removing link by id.
     * @param id Link id
     */
    void removeById(long id);

    /**
     * Removing by person id
     * @param id The person id
     */
    void removeByPersonId(long id);
}
