package ru.jankbyte.webapp.justcut.service.link;

import ru.jankbyte.webapp.justcut.model.link.Link;
import ru.jankbyte.webapp.justcut.model.person.Person;
import ru.jankbyte.webapp.justcut.service.PersonService;
import ru.jankbyte.webapp.justcut.repository.LinkRepository;
import ru.jankbyte.webapp.justcut.exception.notfound.LinkNotFoundException;
import ru.jankbyte.webapp.justcut.component.LongConversion;

import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.time.LocalDateTime;

@Service
public class LinkServiceImpl implements LinkService {
    private final LinkRepository linkRepository;
    private final PersonService personService;
    private final Supplier<LinkNotFoundException> linkNotFoundThrowing =
        LinkNotFoundException::new;


    public LinkServiceImpl(LinkRepository linkRepository,
            @Lazy PersonService personService) {
        this.personService = personService;
        this.linkRepository =  linkRepository;
    }

    @Override
    public Link saveOrUpdate(Link link) {
        return linkRepository.save(link);
    }

    @Override
    public Link getById(long id) {
        return linkRepository.findById(id)
            .orElseThrow(linkNotFoundThrowing);
    }

    @Override
    public Link getByIdForIncressClicks(Long id) {
        return linkRepository.findByIdForIncressClicks(id)
            .orElseThrow(linkNotFoundThrowing);
    }

    @Override
    public List<Link> getByPersonId(long id) {
        return linkRepository.findByPersonId(id);
    }

    @Override
    public long getCount() {
        return linkRepository.count();
    }

    @Override
    public void removeById(long id) {
        linkRepository.deleteById(id);
    }

    @Override
    public boolean containsInPerson(long linkId, long personId) {
        return linkRepository.existsInPerson(linkId, personId);
    }

    @Override
    @Transactional
    public Link createLink(Link link, long personId) {
        Person owner = personService.getById(personId);
        link.setPerson(owner);
        return linkRepository.save(link);
    }

    @Override
    public void removeByPersonId(long id) {
        linkRepository.deleteByPersonId(id);
    }
}
