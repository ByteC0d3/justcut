package ru.jankbyte.webapp.justcut.service.link;

import ru.jankbyte.webapp.justcut.model.link.Link;
/**
 * Provides methods for shorting URL.
 */
public interface LinkShorterService {
    /**
     * Shorting the target URL.
     * @param originalUrl The URL
     * @param personId Person id
     * @return The shorted URL
     */
    String createShortURL(String originalUrl, Long personId);

    /**
     * Getting original URL by shorted.
     * @param shortUrl The shorted URL
     * @return The link entity with URL data
     */
    Link getLinkByShortURL(String shortUrl);
}
