package ru.jankbyte.webapp.justcut.service.link;

import ru.jankbyte.webapp.justcut.component.LongConversion;
import ru.jankbyte.webapp.justcut.model.link.Link;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class LinkShorterServiceImpl implements LinkShorterService {
    private final LongConversion encoder;
    private final LinkService linkService;

    @Override
    public String createShortURL(String originalUrl, Long personId) {
        LocalDateTime now = LocalDateTime.now();
        Link link = Link.builder()
            .created(now).url(originalUrl).build();
        link = linkService.createLink(link, personId);
        Long id = link.getId();
        return encoder.encode(id);
    }

    @Override
    @Transactional
    public Link getLinkByShortURL(String shortUrl) {
        Long id = encoder.decode(shortUrl);
        Link link = linkService.getByIdForIncressClicks(id);
        link.incressClicks();
        return linkService.saveOrUpdate(link);
    }
}
