package ru.jankbyte.webapp.justcut.util;

public final class RegExUtils {
    private RegExUtils() {}

    public static final String EMAIL_REGEX = "^[a-zA-Z.0-9]+@[a-zA-Z]+\\.[a-zA-Z]+$";
    public static final String URL_REGEX = "^(http|https)(:\\/\\/){1}([\\w.-]+(:[0-9]+)*)(\\/[\\w.-]+)*(\\?(\\w+=\\w+&*)*)?\\/*$";

    public static boolean isEmail(String source) {
        return source.matches(EMAIL_REGEX);
    }
}
