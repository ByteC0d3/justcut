package ru.jankbyte.webapp.justcut.validator;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Component;
import ru.jankbyte.webapp.justcut.dto.registration.NewPersonDto;
import ru.jankbyte.webapp.justcut.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class PersonEmailExistsValidator implements Validator {
    public static final String EMAIL_EXISTS_FIELD = "email.exists";
    private final PersonService personService;

    @Override
    public boolean supports(Class<?> clazz) {
        return NewPersonDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        log.debug("Checking existing of email for new person");
        NewPersonDto dto = (NewPersonDto) target;
        String email = dto.email();
        boolean isExistsByEmail = personService.existsByEmail(email);
        if (!isExistsByEmail) {
            return;
        }
        errors.reject(EMAIL_EXISTS_FIELD, "User with this email exists");
    }
}
