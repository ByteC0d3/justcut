package ru.jankbyte.webapp.justcut.repository;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.testcontainers.containers.PostgreSQLContainer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.containers.GenericContainer;

@Testcontainers
public class AccountRepositoryTest {

    @Container
    private final GenericContainer postgreSQLContainer =
        new PostgreSQLContainer("postgres:15.3")
            .withDatabaseName("justcut_test")
            .withPassword("jankbyte")
            .withUsername("jankbyte")
            .withExposedPorts(5432);

    @Test
    public void test() {
        System.out.println(((PostgreSQLContainer) postgreSQLContainer).getJdbcUrl());
    }
}
