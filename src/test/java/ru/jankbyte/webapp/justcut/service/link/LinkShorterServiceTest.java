package ru.jankbyte.webapp.justcut.service.link;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.Spy;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.ArgumentMatchers;

import ru.jankbyte.webapp.justcut.test.AbstractMockitoTest;
import ru.jankbyte.webapp.justcut.component.LongConversion;
import ru.jankbyte.webapp.justcut.security.user.UserAccount;
import ru.jankbyte.webapp.justcut.model.link.Link;

public class LinkShorterServiceTest extends AbstractMockitoTest {
    @Mock
    private LinkService linkService;

    @Spy
    private final LongConversion encoder = new LongConversion();

    @InjectMocks
    private LinkShorterServiceImpl linkShorterService;

    @DisplayName("Should create short URL")
    @ParameterizedTest
    @CsvSource({
        "100, 'bM'", "100000, 'Aa4'", "432764527, 'DrZZb'"
    })
    public void shouldCreateShortURL(Long id, String expected) {
        mockingGetShortURLMethod(id);
        String shortUrl = linkShorterService.createShortURL("test", id);
        assertThat(shortUrl).isEqualTo(expected);
    }

    @DisplayName("Should get link by shortcut")
    @ParameterizedTest
    @CsvSource({
        "100, 'bM', 'https://google.com'",
        "100000, 'Aa4', 'https://google1.com'",
        "432764527, 'DrZZb', 'https://google2.com'"
    })
    public void shouldGetLinkByShortCutURL(Long id,
            String shortUrl, String urlExpected) {
        mockingGetLinkByShortURLMethod(id, urlExpected);
        Link link = linkShorterService.getLinkByShortURL(shortUrl);
        assertThat(link).isNotNull()
            .extracting("id", "url")
            .contains(id, urlExpected);
    }

    private void mockingGetShortURLMethod(long id) {
        Link link = Link.builder().build();
        link.setId(id);
        Mockito.when(linkService.createLink(
            ArgumentMatchers.any(), ArgumentMatchers.anyLong()))
                .thenReturn(link);
    }

    private void mockingGetLinkByShortURLMethod(Long id, String url) {
        Link link = Link.builder().url(url).build();
        link.setId(id);
        Mockito.when(linkService.getByIdForIncressClicks(id))
            .thenReturn(link);
        Mockito.when(linkService.saveOrUpdate(link))
            .thenReturn(link);
    }
}
