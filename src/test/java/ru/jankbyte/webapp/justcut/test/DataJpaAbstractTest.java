package ru.jankbyte.webapp.justcut.test;

import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.ContextConfiguration;

import jakarta.persistence.PersistenceContext;
import jakarta.persistence.EntityManager;

import ru.jankbyte.webapp.justcut.test.LoggingAbstractTest;

/**
 * The base test for repositories and service data saving.
 * <p></p>
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public abstract class DataJpaAbstractTest extends LoggingAbstractTest {
    @PersistenceContext
    protected EntityManager entityManager;
}
