package ru.jankbyte.webapp.justcut.test;

import java.util.logging.Logger;

/**
 * The base class for enabling logger.
 */
public abstract class LoggingAbstractTest {
    protected final Logger log = Logger.getLogger(getClass().getName());
}
