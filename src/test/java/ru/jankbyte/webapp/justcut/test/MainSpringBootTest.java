package ru.jankbyte.webapp.justcut.test;

import org.springframework.boot.test.context.SpringBootTest;
import ru.jankbyte.webapp.justcut.Launcher;

@SpringBootTest(classes = Launcher.class)
public abstract class MainSpringBootTest extends LoggingAbstractTest {
}
