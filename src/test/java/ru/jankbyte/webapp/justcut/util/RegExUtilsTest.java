package ru.jankbyte.webapp.justcut.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.assertj.core.api.Assertions.assertThat;

public class RegExUtilsTest {
    @ParameterizedTest
    @CsvSource({
        "test@gmail.com, true", "malcolm.holms@gmail.com, true",
        "test.gmail.com, false", "malfoy.bran@.com, false"
    })
    public void shouldValidateEmail(String email, boolean valid) {
        boolean isValid = RegExUtils.isEmail(email);
        assertThat(isValid).isEqualTo(valid);
    }
}
