package ru.jankbyte.webapp.justcut.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.assertj.core.api.Assertions.assertThat;
import jakarta.validation.Validator;
import jakarta.validation.ConstraintViolation;
import java.util.Set;

import ru.jankbyte.webapp.justcut.dto.link.NewLink;

@SpringBootTest(classes = ValidationAutoConfiguration.class)
public class IsURLAnnotationValidationTest {
    @Autowired
    private Validator validator;

    @DisplayName("Should validate URL's")
    @ParameterizedTest
    @CsvSource({
        "'http://google.com', true",
        "'https://sample.com/dfsdf/hello?name=alex&password=999', true",
        "'https://sample.com/dfsdf/hello?name=alex', true",
        "'http://myurl.sample:9090/hello-world', true",
        "'http://my-url.sam-ple:9090/hello_world', true",
        "'https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test/3.1.0', true"
    })
    public void shouldValidateURLs(String url, boolean isValid) {
        NewLink link = new NewLink(url);
        Set<ConstraintViolation<NewLink>> violations = validator.validate(link);
        boolean isValidatedSuccess = violations.isEmpty();
        assertThat(isValidatedSuccess).isEqualTo(isValid);
    }
}
