-- add managment accounts
CALL develop.create_account('deadly', 'admin123', '{"MANAGE_ACCOUNTS"}', true);
CALL develop.create_account('max', 'max123', '{"MANAGE_ACCOUNTS", "MANAGE_ALL_LINKS"}', true);

-- add persons
CALL develop.create_person('baby2001@gmail.com', 'haha123', '{"CREATE_LINKS"}', true);
CALL develop.create_person('jankov@gmail.com', 'hjfgjhgf765fgsdfgS#', '{"CREATE_LINKS"}', true);

-- adding links
CALL develop.create_link('baby2001@gmail.com', 'http://t.me/babik');
CALL develop.create_link('baby2001@gmail.com', 'http://vk.com/babik');
